package com.azserve.javacode.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class DbPostgres {

	public static void main(final String[] args) throws Exception {

		try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test", "test", "test");
				Statement statement = connection.createStatement()) {

			try (ResultSet rs = statement.executeQuery("select * from prova")) {
				while (rs.next()) {
					System.out.println(rs.getInt(1) + " " + rs.getString(2));
				}
			}

			try (PreparedStatement ps = connection.prepareStatement("select * from prova where descrizione like ?")) {
				ps.setString(1, "%e");
				try (ResultSet rs = ps.executeQuery()) {
					while (rs.next()) {
						System.out.println(rs.getInt(1) + " " + rs.getString(2));
					}
				}
			}

			System.out.println("Completato con successo");
		}
	}
}
