package com.azserve.javacode.server.webapi.rs;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.azserve.javacode.server.ejb.bl.TestService;
import com.azserve.javacode.server.ejb.dto.UserQDTO;

@Path("test")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Test", description = "Test api")
public class TestApi {

	@Inject
	private TestService testService;

	@GET
	@Path("insert")
	@Operation(summary = "doSomething1", description = "doSomething1 method")
	public UUID insert(@QueryParam("username") final String username, @QueryParam("id-comune") final String idComune) {
		return this.testService.insert(username, idComune);
	}

	@GET
	@Path("find")
	@Operation(summary = "doSomething2", description = "doSomething2 method")
	public List<UserQDTO> find() {
		return this.testService.find();
	}
}