package com.azserve.javacode.server.ejb.bl;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.azserve.javacode.server.ejb.EEConstants;
import com.azserve.javacode.server.ejb.dto.UserQDTO;
import com.azserve.javacode.server.ejb.entity.Comune;
import com.azserve.javacode.server.ejb.entity.User;

@Stateless
@LocalBean
public class TestService {

	@PersistenceContext(unitName = EEConstants.PERSISTENCE_UNIT)
	private EntityManager em;

	@EJB
	private SecondService secondService;

	public UUID insert(final String username, final String idComune) {
		final UUID id = UUID.randomUUID();
		final User user = new User();
		user.setId(id);
		user.setUsername(username);
		user.setActive(true);
		if (idComune != null) {
			final Comune referenceComune = this.em.getReference(Comune.class, idComune);
			user.setComuneNascita(referenceComune);
		}
		// this.secondService.insertRandomComune();
		this.em.flush();
		this.em.persist(user);
		return id;
	}

	public List<UserQDTO> find() {
		final CriteriaBuilder cb = this.em.getCriteriaBuilder();
		final CriteriaQuery<User> cr = cb.createQuery(User.class);
		final Root<User> root = cr.from(User.class);
		cr.select(root);

		final TypedQuery<User> query = this.em.createQuery(cr);

		final List<UserQDTO> result = query.getResultStream()
				.map(q -> new UserQDTO(q.getId(), q.getUsername(), q.isActive(), Optional.ofNullable(q.getComuneNascita()).map(c -> c.getNome()).orElse("sconosciuto")))
				.collect(toList());
		return result;
	}
}
