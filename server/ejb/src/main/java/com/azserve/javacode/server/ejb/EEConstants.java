package com.azserve.javacode.server.ejb;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.BinaryDataStrategy;
import javax.json.bind.config.PropertyVisibilityStrategy;

public final class EEConstants {

	public static final String PERSISTENCE_UNIT = "server";

	public static final Jsonb JSONB = JsonbBuilder.newBuilder().withConfig(new JsonbConfig()
			.withBinaryDataStrategy(BinaryDataStrategy.BASE_64)
			.withPropertyVisibilityStrategy(new PropertyVisibilityStrategy() {

				@Override
				public boolean isVisible(final Method method) {
					return false;
				}

				@Override
				public boolean isVisible(final Field field) {
					return true;
				}
			})).build();

	private EEConstants() {}
}
