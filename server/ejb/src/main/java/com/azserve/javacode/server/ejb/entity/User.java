package com.azserve.javacode.server.ejb.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "users", indexes = @Index(columnList = "username", unique = true, name = "UX_USER_USERNAME"))
public class User {

	@Id
	private UUID id;

	@Column(nullable = false)
	private String username;

	private boolean active;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_USERS_COMUNENASCITA"))
	private Comune comuneNascita;

	public UUID getId() {
		return this.id;
	}

	public void setId(final UUID id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(final boolean active) {
		this.active = active;
	}

	public Comune getComuneNascita() {
		return this.comuneNascita;
	}

	public void setComuneNascita(final Comune comuneNascita) {
		this.comuneNascita = comuneNascita;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.id == null ? 0 : this.id.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final User other = (User) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}
}
