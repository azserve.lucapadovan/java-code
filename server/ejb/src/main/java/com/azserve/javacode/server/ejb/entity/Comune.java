package com.azserve.javacode.server.ejb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Comune {

	@Id
	@Column(length = 6)
	private String id;

	private String nome;

	public Comune() {}

	public Comune(final String id, final String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

}
