package com.azserve.javacode.server.ejb.dto;

import java.util.UUID;

public class UserQDTO {

	private UUID id;
	private String username;
	private boolean active;
	private String comuneNascita;

	public UserQDTO() {
		super();
	}

	public UserQDTO(final UUID id, final String username, final boolean active, final String comuneNascita) {
		super();
		this.id = id;
		this.username = username;
		this.active = active;
		this.comuneNascita = comuneNascita;
	}

	public UUID getId() {
		return this.id;
	}

	public void setId(final UUID id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(final boolean active) {
		this.active = active;
	}

	public String getComuneNascita() {
		return this.comuneNascita;
	}
}
