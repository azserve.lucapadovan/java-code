package com.azserve.javacode.server.ejb.bl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.azserve.javacode.server.ejb.EEConstants;
import com.azserve.javacode.server.ejb.entity.Comune;
import com.azserve.javacode.server.ejb.entity.Comune_;

@Stateless
@LocalBean
public class SecondService {

	@PersistenceContext(unitName = EEConstants.PERSISTENCE_UNIT)
	private EntityManager em;

	public void insertRandomComune() {
		final CriteriaBuilder cb = this.em.getCriteriaBuilder();
		final CriteriaQuery<Comune> cr = cb.createQuery(Comune.class);
		final Root<Comune> root = cr.from(Comune.class);
		cr.select(root);
		cr.orderBy(cb.desc(root.get(Comune_.id)));

		final TypedQuery<Comune> query = this.em.createQuery(cr);

		query.setFirstResult(0);
		query.setMaxResults(1);
		final Comune comune = query.getResultList().get(0);
		final int next = Integer.valueOf(comune.getId()).intValue() + 1;
		final Comune c = new Comune(next + "", "nome a caso");
		this.em.persist(c);

	}

}
