package com.azserve.javacode.server.ejb.bl;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.azserve.javacode.server.ejb.EEConstants;
import com.azserve.javacode.server.ejb.entity.Comune;

@Singleton
@Startup
public class InitService {

	@PersistenceContext(unitName = EEConstants.PERSISTENCE_UNIT)
	private EntityManager em;

	@PostConstruct
	private void init() {
		final Comune c1 = new Comune("000001", "San donà di Piave");
		this.em.persist(c1);
		final Comune c2 = new Comune("000002", "Salgareda");
		this.em.persist(c2);
		final Comune c3 = new Comune("000003", "Noventa di Piave");
		this.em.persist(c3);
	}
}
