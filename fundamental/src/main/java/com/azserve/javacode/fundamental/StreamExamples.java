package com.azserve.javacode.fundamental;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

public class StreamExamples {

	public static void main(final String[] args) {

		final List<String> list = List.of("uno", "due", "tre", "quattro", "cinque");

		final List<Integer> lengths = list.stream()
				.filter(s -> s.length() > 4) // come Filtro
				.sorted()
				.map(s -> Integer.valueOf(s.length()))
				.collect(toList());

		System.out.println(lengths);

		final Set<String> set = Set.of("uno", "due", "tre", "quattro", "cinque");
		for (final String string : set) {
			System.out.println(string);
		}

		final Map<String, String> map = Map.of("Uno", "1", "Due", "2", "Tre", "3");
		final String result = map.get("Uno");

		System.out.println(result);

	}

	private static class Filtro implements Predicate<String> {

		@Override
		public boolean test(final String t) {
			return t.length() > 4;
		}

	}
}
